//
//  MyLocation.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 23.02.2023.
//

import Foundation
import CoreLocation

protocol MyLocationDelegate {
    func receivedCurrentLocation(coord: CLLocationCoordinate2D?)
}

class MyLocation: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var delegate: MyLocationDelegate?
    
    func getCurrentLocation() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        if locationManager.authorizationStatus == .authorizedWhenInUse {
            DispatchQueue.global(qos: .userInitiated).async {
                self.locationManager.requestLocation()
            }
        }
    }
    
    //MARK: CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print(error.localizedDescription)
        DispatchQueue.main.async {
            self.delegate?.receivedCurrentLocation(coord: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("update location")
        if let location = locations.last {
            locationManager.stopUpdatingLocation()
            print(location.coordinate)
            DispatchQueue.main.async {
                self.delegate?.receivedCurrentLocation(coord: location.coordinate)
            }
        }
    }
    
    
    //MARK: - First launch (после одобрения пользователем использования геолокации)
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
}
