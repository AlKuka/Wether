//
//  CoreDataHelper.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 01.03.2023.
//

import UIKit
import CoreData


let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

// MARK: - Spot Entity
/**
 функция сохраняет населённый пункт
 - Parameters:
    - moc: managedObjectContext
    - city: название города
    - country: страна
    - lat: latitude
    - lon: longitude
 */
public func saveSpot(_ moc: NSManagedObjectContext, city: String, country: String, lat: Float, lon: Float) {
    let spot : Spot
    //проверяем что уже сохранён этот инструмент
    if let old = getSpot(moc, city: city, country: country){
        spot = old
    } else {
        spot = Spot(context: moc)
        spot.city = city
        spot.country = country
    }
    spot.lat = lat
    spot.lon = lon
}


/**
 функция ищет по названию в базе данных, и если есть - возвращает город
 - Parameters:
    - moc: managedObjectContext
    - city: название города
 - Returns: возвращает сущность Spot или nil
 */
public func getSpot(_ moc: NSManagedObjectContext, city: String, country: String) -> Spot? {
    let request = Spot.fetchRequest()
    request.predicate = NSPredicate(format: "city = %@ AND country = %@", argumentArray: [city, country])
    if let result = try? moc.fetch(request), result.count > 0 {
        return result[0]
    }else{
        return nil
    }
}


/**
 функция ищет в базе данных, и если есть - возвращает сохранённые населённые пункты
 - Parameters:
    - moc: managedObjectContext
 - Returns: возвращает cохранённые Spots
 */
public func getStoredCitys(_ moc: NSManagedObjectContext) -> [Spot]? {
    let request = Spot.fetchRequest()
    request.sortDescriptors = [NSSortDescriptor(key: "city", ascending: true)]
    if let result = try? moc.fetch(request), result.count > 0 {
        return result
    }else{
        return nil
    }
}


public func getSpots(_ moc: NSManagedObjectContext, prefix: String) -> [Spot]? {
    let request = Spot.fetchRequest()
    request.predicate = NSPredicate(format: "city BEGINSWITH[cd] %@", prefix)
    request.sortDescriptors = [NSSortDescriptor(key: "city", ascending: true)]
    if let result = try? moc.fetch(request), result.count > 0 {
        return result
    }else{
        return nil
    }
}
