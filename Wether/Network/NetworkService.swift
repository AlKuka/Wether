//
//  NetworkService.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 17.02.2023.
//

import Foundation

protocol NetworkServiceDelegate {
    func getCurrentWeather(lat: Float, lon: Float, result: @escaping (CurrentWeather?) -> Void)
    func get5DaysForecast(lat: Float, lon: Float, result: @escaping (JsonForecast?) -> Void)
    func getCitysCollection(name: String, result: @escaping ([City]?) -> Void)
}


class NetworkService {
    
    private let domain = "https://api.openweathermap.org"
    var latitude : Float = 51.5085
    var longitude : Float = -0.1257
    var cityName = "London"
    

    private enum UrlItems : String {
        case fiveDays = "/data/2.5/forecast?",
             current = "/data/2.5/weather?",
             geocoding = "/geo/1.0/direct?"
    }
    
    private enum UrlParams: String {
        case lat = "lat",
        lon = "lon",
        appid = "appid", //APIkey
        city = "q" //City name, state code (only for the US) and country code divided by comma. Please use ISO 3166 country codes.
    }
    
    
    //MARK: - Data from URL
    private func getData(item: UrlItems, result: @escaping (Data?) -> Void) {
        let session = URLSession.shared
        let request = getUrlRequest(item: item)
        print(request.url!.absoluteString)
        session.dataTask(with: request, completionHandler: { (data, response, error) in
            DispatchQueue.main.async {
                if error != nil || response == nil {
                    print(error!.localizedDescription)
                    result(nil)
                } else {
                    result(data)
                }
            }
        }).resume()
    }
    
    
    //MARK: - URLRequest
    private func getUrlRequest(item: UrlItems) -> URLRequest {
        let address = domain + item.rawValue
        guard var url = URL(string: address) else {
            fatalError("incorrect URL address")
        }
        switch item {
        case .fiveDays, .current:
            url.appendQueryItem(name: UrlParams.lat.rawValue, value: "\(latitude)")
            url.appendQueryItem(name: UrlParams.lon.rawValue, value: "\(longitude)")
            url.appendQueryItem(name: UrlParams.appid.rawValue, value: APIkey)
        case .geocoding:
            url.appendQueryItem(name: UrlParams.city.rawValue, value: cityName)
            url.appendQueryItem(name: UrlParams.appid.rawValue, value: APIkey)
        }
        let request = URLRequest(url: url)
        
        return request
    }
}


//MARK: - Delegate
extension NetworkService: NetworkServiceDelegate {
    func getCurrentWeather(lat: Float, lon: Float, result: @escaping (CurrentWeather?) -> Void) {
        latitude = lat
        longitude = lon
        getData(item: .current) { data in
            if data != nil,
               let answer = try? JSONDecoder().decode(CurrentWeather.self, from: data!) {
                result(answer)
            } else {
                result(nil)
            }
        }
    }
    
    func get5DaysForecast(lat: Float, lon: Float, result: @escaping (JsonForecast?) -> Void) {
        latitude = lat
        longitude = lon
        getData(item: .fiveDays) { data in
            if data != nil,
               let answer = try? JSONDecoder().decode(JsonForecast.self, from: data!) {
                result(answer)
            } else {
                result(nil)
            }
        }
    }
    
    func getCitysCollection(name: String, result: @escaping ([City]?) -> Void) {
        cityName = name
        getData(item: .geocoding) { data in
            if data != nil,
               let answer = try? JSONDecoder().decode([City].self, from: data!) {
                result(answer)
            } else {
                result(nil)
            }
        }
    }
}


//MARK: - Query Keys
extension URL {
    mutating func appendQueryItem(name: String, value: String?) {
        guard var urlComponents = URLComponents(string: absoluteString) else { return }
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        let queryItem = URLQueryItem(name: name, value: value)
        queryItems.append(queryItem)
        urlComponents.queryItems = queryItems
        self = urlComponents.url!
    }
}
