//
//  JsonDecodable.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 22.02.2023.
//

import Foundation


//MARK: Forecast
public struct JsonForecast: Decodable {
    var cnt: Int?
    var list: [ForecastList]
    var city: ForecastCity?
}

struct ForecastList: Decodable {
    var dt: Double
    var main: ForecastMain
    var weather: [ForecastIcon]
    var clouds: ForecastClouds
    var wind: ForecastWind
    var rain: [String: Float]? // 3h - Rain volume for the last 3 hour, mm
    var snow: [String: Float]? // 3h - Snow volume for the last 3 hour, mm
}

//MARK: - Current Weather
struct CurrentWeather: Decodable {
    var weather: [ForecastIcon]
    var main: ForecastMain
    var wind: ForecastWind
    var clouds: ForecastClouds
    var rain: [String: Float]? // 1h(3h) - Rain volume for the last 1(3) hour, mm
    var snow: [String: Float]? // 1h(3h) - Snow volume for the last 1(3) hour, mm
    var dt: Double //Time of data calculation, unix, UTC
}


struct ForecastCity: Decodable {
    var name: String?
    var coord: ForecastCoord
}

struct ForecastCoord: Decodable {
    var lat: Float
    var lon: Float
}

struct ForecastWind: Decodable {
    var speed: Float? //Wind speed. Unit Default: meter/sec
    var deg: Float? //Wind direction, degrees (meteorological)
    var gust: Float? //порывы ветра
}

struct ForecastClouds: Decodable {
    var all: Float?  //Cloudiness, %
}

struct ForecastMain: Decodable {
    var temp: Float? //Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
    var humidity: Int? //Humidity %
}

struct ForecastIcon: Decodable {
    var main: String? //Group of weather parameters (Rain, Snow, Extreme etc.)
    var description: String? //Weather condition within the group
    var icon: String?
}


//MARK: - City names
//struct LocalNames: Codable {
//    var ru: String?
//    var ua: String?
//}

struct City: Codable {
    var name: String
    var local_names: [String: String]?
    var lat: Float
    var lon: Float
    var country: String
    var state: String?
}
