//
//  public.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 17.02.2023.
//

import UIKit


let APIkey = "387c4f499ef5274c0998ebb350f51190" //API OpenWeatherMap
var celsiumUnits = true


//MARK: - Segue Id
public enum SegueId: String {
    case mapSegue, citySearchSegue
}


//MARK: - Wind Directions
public func getWindDirectionFromDegree(_ deg: Float) -> String {
    
    if (deg > 337.5) || (deg <= 22.5) { // N
        return "↓"
    } else if (deg > 22.5) && (deg <= 67.5) { // NE
        return "↙︎"
    }else if (deg > 67.5) && (deg <= 112.5) { // E
        return "←"
    } else if (deg > 112.5) && (deg <= 157.5) { // SE
        return "↖︎"
    } else if (deg > 157.5) && (deg <= 202.5) { // S
        return "↑"
    } else if (deg > 202.5) && (deg <= 247.5) { // SW
        return "↗︎"
    } else if (deg > 247.5) && (deg <= 292.5) { // W
        return "→"
    } else if (deg > 292.5) && (deg <= 337.5) { // NW
        return "↘︎"
    } else {
        return "x"
    }
} //направление ветра


//MARK: - weather Icons
public func getWeatherIcon (_ icon: String) -> String {
    switch icon {
    case "01d":
        return "☀️"
    case "02d", "02n":
        return "⛅️"
    case "03d", "03n":
        return "☁️"
    case "04d", "04n":
        return "☁️"
    case "09d", "09n":
        return "💧💧"
    case "10d", "10n":
        return "💧"
    case "11d", "11n":
        return "⚡️"
    case "13d", "13n":
        return "❄️"
    case "01n":
        return "🌙"
    case "50d", "50n":
        return "♒︎"
    default:
        return ""
    }
}


//MARK: - Temperature Units
public func celsiumToFarenheit(_ cels: Float) -> Float {
    let farnh = cels * 1.8 + 32
    return farnh
}


//MARK: - Locale values
public func getTempLocale(kelv: Float) -> (value: Float, text: String) {
    var value = kelv - 273.15 //перевод из Кельвинов в Цельсии
    var text = formatChislo(roundf(value), f: 0) + "°C"
    if !Locale.current.usesMetricSystem {
        value = celsiumToFarenheit(value)
        text = formatChislo(roundf(value), f: 0) + "°F"
    }
    return (value, text)
}

public func getWindLocale(mps: Float, direction: Float) -> String {
    var units = "m/s"
    var windResult = mps
    if !Locale.current.usesMetricSystem {
        units = "kn"
        windResult = mps * 1.94
    }
    let wind = "\(Int(round(windResult)))" + units + " " + getWindDirectionFromDegree(direction)
    return wind
}

public func speedLocale(kmh: Float) -> String {
    if Locale.current.usesMetricSystem {
        return formatChislo(kmh, f: 1) + " km/h"
    }else{
        return formatChislo(kmh * 0.54, f: 1) + " kn"
    }
}

public func distanceLocale(m: Int) -> String {
    if Locale.current.usesMetricSystem {
        if m < 1000 {
            return "\(m) m"
        }else{
            return "\(formatChislo(Float(m) / 1000, f: 2)) km"
        }
    }else{
        let mi = Float(m) * 0.00054
        if mi < 1 {
            return "\(formatChislo(mi, f: 3)) nmi"
        }else if mi < 10{
            return "\(formatChislo(mi, f: 2)) nmi"
        }else{
            return "\(formatChislo(mi, f: 1)) nmi"
        }
    }
}


//MARK: Float -> String
public func formatChislo (_ chislo :Float, f:Int) -> String {
    let a = NumberFormatter()
    a.numberStyle = NumberFormatter.Style.decimal
    a.maximumFractionDigits = f
    let result = "\(a.string(from: chislo as NSNumber)!)"
    return result
}


//MARK: - Radians convertor
public func radiansToDegrees(radians: Double) -> Double {
    return radians * 180 / Double.pi
}

public func degreesToRadians(degrees: Double) -> Double {
    return degrees * Double.pi / 180
}


//MARK: - DAY of Week
/**
 функция возвращает день недели
 - Parameters:
    - date: дата в формате Double
    - todayOn: если true то проверяет и возвращает при совпадении сегодня / завтра / вчера
 - Returns: возвращает String название дня недели
 */
public func getDayOfWeak (_ date: Date, todayOn: Bool) -> String {
    let calendar = Calendar.current
    let today = calendar.component(.day, from: Date())
    let tomorrow = calendar.component(.day, from: Date().addingTimeInterval(24*60*60))
    let yesterday = calendar.component(.day, from: Date().addingTimeInterval(-24*60*60))
    let itemDay = calendar.component(.day, from: date)
    let dateFormatter = DateFormatter()
    if todayOn {
        dateFormatter.dateFormat = "EEEE"
    } else {
        dateFormatter.dateFormat = "EE"
    }
    let day = dateFormatter.string(from: date)
    let ls0 = NSLocalizedString("TODAY_DAY", comment: "today")
    let ls1 = NSLocalizedString("TOMORROW_DAY", comment: "tomorrow")
    let ls2 = NSLocalizedString("YESTERDAY_DAY", comment: "yesterday")
    if !todayOn {
        return day
    } else if itemDay == today {
        return ls0
    } else if itemDay == tomorrow {
        return ls1
    } else if itemDay == yesterday {
        return ls2
    } else {
        return day
    }
}

public func getHourFromDate(date: Double) -> String {
    let calendar = Calendar.current
    let hour = calendar.component(.hour, from: Date(timeIntervalSince1970: date))
    return "\(hour)⁰⁰"
}


// MARK: - Text Alert
/**
 функция позволяет получить для презентации стандартное окно сообщения с одной кнопкой закрытия ОК
 - Parameters:
    - title: текст заголовка
    - message: текст сообщения
 - Returns: возвращает готовый к презентации UIAlertController
 */
public func getTextMessageAlert(title: String?, message: String?) -> UIAlertController {
    let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "OK",
                                  style: UIAlertAction.Style.default, handler: nil))
    alert.modalPresentationStyle = .overCurrentContext
    return alert
}


//MARK: - Bool / Int
extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}

extension Int {
    var boolValue: Bool { return self != 0 }
}
