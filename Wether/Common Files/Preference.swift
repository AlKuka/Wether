//
//  Preference.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 01.03.2023.
//


import UIKit

let defaults = UserDefaults.standard

// MARK: - Настройки
struct Preferences {
    
    static var hideMapHelp: Bool {
        get {
            return defaults.bool(forKey: .hideMapHelp)
        }
        set {
            defaults.set(newValue, forKey: .hideMapHelp)
            defaults.synchronize()
        }
    }
    
    static var maxSizeImage: Int {
        get {
            return defaults.integer(forKey: .maxSizeImage)
        }
        set {
            defaults.set(newValue, forKey: .maxSizeImage)
            defaults.synchronize()
        }
    }
    
    
    static func restore() {
        Preferences.hideMapHelp = false
    }
}


// MARK: - Ключи defaults
extension UserDefaults {
    enum Key: String {
        case hideMapHelp = "hideMapHelp"
        case maxSizeImage = "maxSizeImage"
    }
    
    func set<T>(_ value: T, forKey key: Key) {
        set(value, forKey: key.rawValue)
    }
    
    func bool(forKey key: Key) -> Bool {
        return bool(forKey: key.rawValue)
    }
    
    func string(forKey key: Key) -> String? {
        return string(forKey: key.rawValue)
    }
    
    func integer(forKey key: Key) -> Int {
        return integer(forKey: key.rawValue)
    }
    
    func float(forKey key: Key) -> Float {
        return float(forKey: key.rawValue)
    }
}
