//
//  ViewController.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 17.02.2023.
//

import UIKit
import CoreLocation

class MainScreenVC: UIViewController {
    
    @IBOutlet weak var daylyWeatherTable: UITableView!
    @IBOutlet weak var hourlyWeatherCollection: UICollectionView!
    @IBOutlet weak var spotName: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var skyIcon: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var wetnessLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var hourlyCollectionHeight: NSLayoutConstraint!
    
    typealias HourWeather = (hour: String, icon: String, temp: Float, humidity: Int, wind: Float, windDirection: Float)
    
    private let weatherService = NetworkService()
    private let dateFormatter = DateFormatter()
    private let locationManager = MyLocation()
    private var weatherCollection: [(day: Date, hourlyWeatherData: [HourWeather])] = []
    private var itemSelected: Int? {
        didSet {
            hourlyWeatherCollection.reloadData()
            if itemSelected == nil && hourlyCollectionHeight.constant != 0 {
                UIView.animate(withDuration: 0.2) {
                    self.hourlyCollectionHeight.constant = 0
                    self.view.layoutIfNeeded()
                }
            } else if hourlyCollectionHeight.constant == 0 {
                UIView.animate(withDuration: 0.4) {
                    self.hourlyCollectionHeight.constant = 140
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preloadConfigureView()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .long
        dateFormatter.setLocalizedDateFormatFromTemplate("MMMM dd")
        dateFormatter.locale = .current
        indicator.startAnimating()
        getCurrentLocation()
    }
    
    
    private func preloadConfigureView() {
        spotName.setTitle("", for: .normal)
        dateLabel.text = "...weather data is loading"
        skyIcon.text = ""
        temperatureLabel.text = ""
        wetnessLabel.text = ""
        windLabel.text = ""
        UIView.animate(withDuration: 0.2) {
            self.hourlyCollectionHeight.constant = 0
        }
    }


    private func getCurrentLocation() {
        locationManager.delegate = self
        locationManager.getCurrentLocation()
    }
    
    
    //MARK: - Weather Screen
    private func setCurrentWeatherScreen(name: String?, day: String, icon: String, temp: String, humidity: Int?, wind: Float?, windDirection: Float?) {
        if name != nil {
            spotName.setTitle(name, for: .normal)
        }
        dateLabel.text = day
        skyIcon.text = icon
        temperatureLabel.text = "🌡 " + temp
        wetnessLabel.text = humidity != nil ? "💦 \(humidity!)%" : ""
        windLabel.text = wind != nil && windDirection != nil ? "💨 " + getWindLocale(mps: wind!, direction: windDirection!) : "no wind"
    }
    
    
    private func showSelectedDayWeather(selectedHour: Int?) {
        guard itemSelected != nil,
              weatherCollection.count > itemSelected! else { return }
        
        let item = weatherCollection[itemSelected!]
        let dayString = getDayOfWeak(item.day, todayOn: true) + ", " + dateFormatter.string(from: item.day)
        if selectedHour != nil {
            let hourData = item.hourlyWeatherData[selectedHour!]
            let icon = getWeatherIcon(hourData.icon)
            let temp = getTempLocale(kelv: hourData.temp)
            let humi = hourData.humidity
            let dayAndHour = dayString + ", " + hourData.hour
            setCurrentWeatherScreen(name: nil, day: dayAndHour, icon: icon, temp: temp.text, humidity: humi, wind: hourData.wind, windDirection: hourData.windDirection)
            
        } else {
            let icon = getSummaryIcon(dayData: item.hourlyWeatherData)
            let temp = getMaxAndMinTemp(dayData: item.hourlyWeatherData)
            let wind = getMaxWind(dayData: item.hourlyWeatherData)
            let humi = getAverageHumidity(dayData: item.hourlyWeatherData)
            setCurrentWeatherScreen(name: nil, day: dayString, icon: icon, temp: temp.text, humidity: humi, wind: wind.wind, windDirection: wind.direction)
        }
    }
    
    
    //MARK: - Geocoder
    //Get City name by geolocation
    private func getGeocoderData(coord: CLLocationCoordinate2D) {
        DispatchQueue.global(qos: .userInteractive).async {
            CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: coord.latitude, longitude: coord.longitude), preferredLocale: Locale.current) { place, error in
                
                guard error == nil else {
                    print(error!.localizedDescription)
                    return
                }
                
                DispatchQueue.main.async {
                    saveSpot(managedObjectContext, city: place?.first?.locality ?? "my place", country: place?.first?.country ?? "", lat: Float(coord.latitude), lon: Float(coord.longitude))
                    self.spotName.setTitle(place?.first?.locality, for: .normal)
                }
            }
        }
    }
    
    
    //MARK: - Get Weather
    private func getCurrentWeather(coord: CLLocationCoordinate2D) {
        weatherService.getCurrentWeather(lat: Float(coord.latitude), lon: Float(coord.longitude)) { [self] weather in
            
            self.indicator.stopAnimating()
            
            guard weather != nil,
            weather!.main.temp != nil,
            weather!.weather.first?.icon != nil else {
                print("no weather data!")
                return
            }
            
            let date = Date(timeIntervalSince1970: weather!.dt)
            
            //отображаем текущую погоду вверху экрана
            setCurrentWeatherScreen(name: nil,
                                    day: dateFormatter.string(from: date),
                                    icon: getWeatherIcon(weather!.weather.first!.icon!),
                                    temp: getTempLocale(kelv: weather!.main.temp!).text,
                                    humidity: weather!.main.humidity,
                                    wind: weather!.wind.speed,
                                    windDirection: weather!.wind.deg)
        }
    }
    
    
    //MARK: - Get Forecast
    private func getForecast(coord: CLLocationCoordinate2D) {
        weatherService.get5DaysForecast(lat: Float(coord.latitude), lon: Float(coord.longitude)) { [self] forecast in
            
            self.indicator.stopAnimating()
            
            guard forecast != nil else {
                print("no forecast data!")
                return
            }
            
            let calendar = Calendar.current
            
            //отображаем прогноз в почасовой коллекции и таблице по дням
            weatherCollection = []
            for item in forecast!.list {
                if let icon = item.weather.first?.icon,
                   let temp = item.main.temp,
                   let hum = item.main.humidity,
                   let wind = item.wind.speed,
                   let windDirection = item.wind.deg {
                    let hour = getHourFromDate(date: item.dt)
                    let itemWeather : HourWeather = (hour: hour,
                                                     icon: icon,
                                                     temp: temp,
                                                     humidity: hum,
                                                     wind: wind,
                                                     windDirection: windDirection )
                    let date = Date(timeIntervalSince1970: item.dt)
                    let day = calendar.component(.day, from: date)
//                    print(itemWeather)
                    //добавляем часовой прогноз в коллекцию
                    if !weatherCollection.isEmpty,
                       calendar.component(.day, from: weatherCollection.last!.day) == day {
                        let index = weatherCollection.count - 1
                        weatherCollection[index].hourlyWeatherData.append(itemWeather)
                    } else {
                        weatherCollection.append((day: date, hourlyWeatherData: [itemWeather]))
                    }
                }
            }
            
            daylyWeatherTable.reloadData()
            //если текущей погоды нет вверху экрана, то сразу показываем погоду на первый день
            if skyIcon.text == "",
                daylyWeatherTable.numberOfRows(inSection: 0) > 0 {
                itemSelected = 0
                showSelectedDayWeather(selectedHour: nil)
            }
        }
    }
    
    
    //MARK: - Max & Min tepm
    private func getMaxAndMinTemp(dayData: [HourWeather]) -> (max: Float, min: Float, text: String) {
        guard dayData.count > 0 else {
            return (0, 0, "")
        }
        var maxTemp = dayData.first!.temp
        var minTemp = maxTemp
        for item in dayData {
            if item.temp > maxTemp {
                maxTemp = item.temp
            }
            if item.temp < minTemp {
                minTemp = item.temp
            }
        }
        let localeMax = getTempLocale(kelv: maxTemp)
        let localeMin = getTempLocale(kelv: minTemp)
        return (localeMax.value, localeMin.value, localeMax.text + " / " + localeMin.text)
    }
    
    
    //MARK: - Max Wind
    private func getMaxWind(dayData: [HourWeather]) -> (wind: Float, direction: Float) {
        guard dayData.count > 0 else {
            return (0, 0)
        }
        var maxWind = dayData.first!.wind
        var direction = dayData.first!.windDirection
        for item in dayData {
            if item.wind > maxWind {
                maxWind = item.wind
                direction = item.windDirection
            }
        }
        return (maxWind, direction)
    }
    
    
    //MARK: - Average Humidity
    private func getAverageHumidity(dayData: [HourWeather]) -> Int? {
        guard dayData.count > 0 else {
            return nil
        }
        let humCollection = dayData.map ({ $0.humidity })
        let average = humCollection.reduce(0, + ) / dayData.count
        return average
    }
    
    
    //MARK: - Summary Icon
    //получаем итоговую иконку из коллекции за день
    private func getSummaryIcon(dayData: [HourWeather]) -> String {
        guard dayData.count > 0 else {
            return ""
        }
        var iconSet : Set<String> = []
        for item in dayData {
            iconSet.update(with: item.icon)
        }
        if (iconSet.contains("09d") || iconSet.contains("10d")) && iconSet.contains("13d") {
            return "💧❄️"
        } else  if iconSet.contains("11d") {
            return "⚡️"
        } else  if iconSet.contains("13d") {
            return "❄️"
        } else if iconSet.contains("09d") || iconSet.contains("10d"){
            return "💧"
        } else if iconSet.contains("02d") || (iconSet.contains("01d") && iconSet.count > 1 ) {
            return "⛅️"
        }
        
        for item in iconSet {
            if item.hasSuffix("d") {
                return getWeatherIcon(item)
            }
        }
        
        return getWeatherIcon(iconSet.first ?? "")
    }
    
    
    //MARK: - Navigation
    @IBAction func openMap(_ sender: UIButton) {
//        let alert = UIAlertController(title: "🌎",
//                                      message: "Выбрать новую локацию?",
//                                      preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "Выбрать на карте", style: UIAlertAction.Style.destructive, handler: {_ in
//            self.performSegue(withIdentifier: SegueId.mapSegue.rawValue, sender: nil)
//        }))
//        alert.addAction(UIAlertAction(title: "Поиск города", style: UIAlertAction.Style.destructive, handler: {_ in
//            self.performSegue(withIdentifier: SegueId.citySearchSegue.rawValue, sender: nil)
//        }))
//        alert.addAction(UIAlertAction(title: "Отмена", style: UIAlertAction.Style.cancel, handler: nil))
//        alert.modalPresentationStyle = .overCurrentContext
//        present(alert, animated: true)
        performSegue(withIdentifier: SegueId.mapSegue.rawValue, sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueId.mapSegue.rawValue,
           let vc = segue.destination as? MapVC {
            vc.delegate = self
        } else if segue.identifier == SegueId.citySearchSegue.rawValue,
                    let vc = segue.destination as? SearchTableVC {
            vc.delegate = self
        }
    }
    
    
    @IBAction func openSearchTable(_ sender: UIButton) {
        performSegue(withIdentifier: SegueId.citySearchSegue.rawValue, sender: nil)
    }
    
}


//MARK: Location Delegate
extension MainScreenVC: MyLocationDelegate {
    
    func receivedCurrentLocation(coord: CLLocationCoordinate2D?) {
        
        guard coord != nil else {
            indicator.stopAnimating()
            return
        }
        
        //получаем название текущего города по геокодингу
        getGeocoderData(coord: coord!)
        
        //получаем погоду на данный момент в данной локации
        getCurrentWeather(coord: coord!)
        
        //загружаем прогноз погоды 5days/3h
        getForecast(coord: coord!)
    }
    
}


//MARK: - TableView
extension MainScreenVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherCollection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "daylyCell", for: indexPath) as! DaylyWeatherTableCell
        let dayWeather = weatherCollection[indexPath.row]
        cell.day.text = getDayOfWeak(dayWeather.day, todayOn: false)
        cell.temp.text = getMaxAndMinTemp(dayData: dayWeather.hourlyWeatherData).text
        cell.icon.text = getSummaryIcon(dayData: dayWeather.hourlyWeatherData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        itemSelected = indexPath.row
        //показыыаем выбранный день вверху экрана
        showSelectedDayWeather(selectedHour: nil)
    }
}


//MARK: - CollectionView
extension MainScreenVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if itemSelected != nil,
           weatherCollection.count > itemSelected! {
            return weatherCollection[itemSelected!].hourlyWeatherData.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "hourlyCell", for: indexPath) as! HourlyWeatherCollectionCell
        let dayData = weatherCollection[itemSelected!].hourlyWeatherData
        let item = dayData[indexPath.row]
        cell.hour.text = item.hour
        cell.icon.text = getWeatherIcon(item.icon)
        cell.temp.text = getTempLocale(kelv: item.temp).text
        cell.highlightItem(false)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? HourlyWeatherCollectionCell {
            cell.highlightItem(true)
        }
        //показыыаем выбранный час прогноза вверху экрана
        showSelectedDayWeather(selectedHour: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? HourlyWeatherCollectionCell {
            cell.highlightItem(false)
        }
    }
}


//MARK: - SpotLocationDelegate
extension MainScreenVC: SpotLocationDelegate {
    func getNewSpotLocation(lat: Double, lon: Double) {
        let coord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        preloadConfigureView()
        getGeocoderData(coord: coord)
        getForecast(coord: coord)
    }
}
