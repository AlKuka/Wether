//
//  DaylyWeatherTableCell.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 24.02.2023.
//

import UIKit

class DaylyWeatherTableCell: UITableViewCell {
    
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var icon: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
