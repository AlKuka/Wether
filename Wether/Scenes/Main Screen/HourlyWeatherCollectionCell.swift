//
//  HourlyWeatherCollectionCell.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 24.02.2023.
//

import UIKit

class HourlyWeatherCollectionCell: UICollectionViewCell {
    @IBOutlet weak var hour: UILabel!
    @IBOutlet weak var icon: UILabel!
    @IBOutlet weak var temp: UILabel!
    
    func highlightItem(_ on: Bool) {
        if on {
            hour.textColor = .green
            temp.textColor = .green
        } else {
            hour.textColor = .white
            temp.textColor = .white
        }
    }
}

