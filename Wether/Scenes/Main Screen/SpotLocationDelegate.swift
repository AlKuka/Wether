//
//  SpotLocationDelegate.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 28.02.2023.
//

import Foundation

protocol SpotLocationDelegate {
    func getNewSpotLocation(lat: Double, lon: Double)
}
