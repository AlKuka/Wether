//
//  SpotAnnotation.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 20.02.2023.
//

import Foundation
import MapKit

class SpotAnnotation: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let forecast: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, forecast: String,  coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.forecast = forecast
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
    
    // pinColor
    func pinColor() -> UIColor {
        return .cyan
    }
}
