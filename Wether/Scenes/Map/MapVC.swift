//
//  MapVC.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 20.02.2023.
//

import UIKit
import MapKit

class MapVC: UIViewController {

    @IBOutlet weak var map: MKMapView!
    
    private let searchRadius: CLLocationDistance = 1200
    var location : CLLocation?
    var delegate: SpotLocationDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if location != nil {
            centerMap(10)
        }
        
        //Единственный раз при первом запуске показываем помощь
        if !Preferences.hideMapHelp {
            present(getTextMessageAlert(title: "ℹ️", message: "Для добавления новой локации нажмите в нужном месте и удерживайте палец на карте"), animated: true) {
                Preferences.hideMapHelp = true
            }
        }
    }

    
    //MARK: - Gesture
    @IBAction func addPoint(_ gesture: UIGestureRecognizer) {
        if gesture.state == UIGestureRecognizer.State.began {
            let touchPoint = gesture.location(in: map)
            let newCoord: CLLocationCoordinate2D = map.convert(touchPoint, toCoordinateFrom: map)
//            location = CLLocation(latitude: newCoord.latitude, longitude: newCoord.longitude)
            delegate?.getNewSpotLocation(lat: newCoord.latitude, lon: newCoord.longitude)
            dismiss(animated: true)
        }
    } //запускает процедуру добавления нового погодного спотa при удержании пальца на карте
    
    
    //MARK: - Center map
    func centerMap(_ radius: Double) {
        let coordinateRegion = MKCoordinateRegion(center: location!.coordinate,
                                                  latitudinalMeters: searchRadius * radius, longitudinalMeters: searchRadius * radius)
        map.setRegion(coordinateRegion, animated: true)
    } //центрирует мою текущую позицию
    
    
    //MARK: - EXIT
    @IBAction func closeMap(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
}

extension MapVC: MKMapViewDelegate {
    func mapView(_ map: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? SpotAnnotation {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = map.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView { // 2
                    dequeuedView.annotation = annotation
                    view = dequeuedView
            } else {
                // 3
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
            }
            view.pinTintColor = annotation.pinColor()
            
            return view
        }
        
        return nil
    }
}
