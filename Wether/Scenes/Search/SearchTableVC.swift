//
//  SearchTableVC.swift
//  Wether
//
//  Created by Oleg Levkutnyk on 20.02.2023.
//

import UIKit

class SearchTableVC: UITableViewController {

    @IBOutlet weak var mySearchBar: UISearchBar!
    
    var delegate: SpotLocationDelegate?
    private let weatherService = NetworkService()
    private var cityList : [City] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        mySearchBar.becomeFirstResponder()
    }
    
    
    private func configureView() {
        cityList.removeAll()
        if let citys = getStoredCitys(managedObjectContext) {
            for city in citys {
                let spot = City(name: city.city!, lat: city.lat, lon: city.lon, country: city.country!)
                cityList.append(spot)
            }
        }
    }

    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultCell", for: indexPath)
        let spot = cityList[indexPath.row]
        var content = cell.defaultContentConfiguration()
        content.text = spot.name
        content.secondaryText = spot.country
                
        cell.contentConfiguration = content

        return cell
    }

    
    //MARK: - Editing
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            let spot = cityList[indexPath.row]
            if let spot = getSpot(managedObjectContext, city: spot.name, country: spot.country) {
                managedObjectContext.delete(spot)
            }
            cityList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    
    //MARK: - Select Row
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let spot = cityList[indexPath.row]
        delegate?.getNewSpotLocation(lat: Double(spot.lat), lon: Double(spot.lon))
        dismiss(animated: true)
    }
}


//MARK: - Search Bar
extension SearchTableVC: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard searchBar.text?.count ?? 0 > 0 else { return }
        weatherService.getCitysCollection(name: searchBar.text!) { [self] result in
            if result?.count ?? 0 > 0 {
                cityList.removeAll()
                for item in result! {
                    var city = item.name
                    var country = item.country
                    if let countryName = Locale.current.localizedString(forRegionCode: country) {
                        country = countryName
                    }
                    if let localeCityName = item.local_names?[Locale.current.languageCode ?? "us"] {
                        city = localeCityName
                    }
                    let spot = City(name: city, lat: item.lat, lon: item.lon, country: country)
                    cityList.append(spot)
                }
                tableView.reloadData()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            configureView()
        } else {
            cityList.removeAll()
            if let spots = getSpots(managedObjectContext, prefix: searchText) {
                for city in spots {
                    let spot = City(name: city.city!, lat: city.lat, lon: city.lon, country: city.country!)
                    cityList.append(spot)
                }
            }
        }
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        dismiss(animated: true)
    }
}
